const axios = require( 'axios' );
const chave = {
	publica: 'pk_test_zD9Jq9IoaSx1JVOk',
	privada: 'sk_test_RYwm6wBcMjt387nb'
};

exports.http = ({ method, path, data }) => {
	const options = {
		method,
		url: `https://api.mundipagg.com/core/v1${path}`,
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Basic ' + new Buffer( chave.privada+':' ).toString( 'base64' )
		}
	};

	if ( method !== 'GET' && data ) {
		options.data = data;
	}

	return axios( options );
}