const { http } = require( '../lib/mundipagg.js' );

exports.cadastro = async ( req, res ) => {
	try {
		const planosCriados = {lista: []}
		const { data: dataPlanoMensal } = await http({
			method: 'POST',
			path: '/plans',
			data: {
				name: 'Tuy-SanarFlix-Mensal',
				description: 'SanarFlix Plano Mensal',
				shippable: false,
				payment_methods: [ 'credit_card' ],
				minimum_price: 2450,
				statement_descriptor: 'SanarFlix Assinatura',
				currency: 'BRL',
				interval: 'month',
				interval_count: 1,
				billing_type: 'prepaid',
				items: [
					{
						name: 'Assinatura Mensal',
						description: 'SanarFlix Assinatura Mensal',
						quantity: 1,
						pricing_scheme: {
							"price": 2450,
							"scheme_type": "unit"
						}
					}
				]
			}
		});


		planosCriados.lista.push({
			id: dataPlanoMensal.id,
			name: dataPlanoMensal.name
		});

		console.log( ` --- Plano cadastrado ${dataPlanoMensal.name} ${dataPlanoMensal.id}` );

		const { data: dataPlanoMensal7Dias } = await http({
			method: 'POST',
			path: '/plans',
			data: {
				name: 'Tuy-SanarFlix-Mensal-7Dias',
				description: 'SanarFlix Plano Mensal 7dias',
				shippable: false,
				payment_methods: [ 'credit_card' ],
				minimum_price: 2450,
				statement_descriptor: 'SanarFlix Assinatura',
				currency: 'BRL',
				interval: 'month',
				interval_count: 1,
				trial_period_days: 7,
				billing_type: 'prepaid',
				items: [
					{
						name: 'Assinatura Mensal-7Dias',
						description: 'SanarFlix Assinatura Mensal-7Dias',
						quantity: 1,
						pricing_scheme: {
							"price": 2450,
							"scheme_type": "unit"
						}
					}
				]
			}
		});

		planosCriados.lista.push({
			id: dataPlanoMensal7Dias.id,
			name: dataPlanoMensal7Dias.name
		});

		console.log( ` --- Plano cadastrado ${dataPlanoMensal7Dias.name} ${dataPlanoMensal7Dias.id}` );

		const { data: dataPlanoTrimestral } = await http({
			method: 'POST',
			path: '/plans',
			data: {
				name: 'Tuy-SanarFlix-Trimestral',
				description: 'SanarFlix Plano Trimestral',
				shippable: false,
				payment_methods: [ 'credit_card' ],
				minimum_price: 6990,
				statement_descriptor: 'SanarFlix Assinatura',
				currency: 'BRL',
				interval: 'month',
				interval_count: 3,
				billing_type: 'prepaid',
				items: [
					{
						name: 'Assinatura Trimestral',
						description: 'SanarFlix Assinatura Trimestral',
						quantity: 1,
						pricing_scheme: {
							"price": 6990,
							"scheme_type": "unit"
						}
					}
				]
			}
		});

		planosCriados.lista.push({
			id: dataPlanoTrimestral.id,
			name: dataPlanoTrimestral.name
		});

		console.log( ` --- Plano cadastrado ${dataPlanoTrimestral.name} ${dataPlanoTrimestral.id}` );

		const { data: dataPlanoMensalComBook } = await http({
			method: 'POST',
			path: '/plans',
			data: {
				name: 'Tuy-SanarFlix-Mensal-Yellowbook',
				description: 'SanarFlix Plano Mensal Yellowbook',
				shippable: false,
				payment_methods: [ 'credit_card' ],
				minimum_price: 2450,
				statement_descriptor: 'SanarFlix Assinatura',
				currency: 'BRL',
				interval: 'month',
				interval_count: 1,
				billing_type: 'prepaid',
				items: [
					{
						name: 'Assinatura Mensal-Yellowbook',
						description: 'SanarFlix Assinatura Mensal-Yellowbook',
						quantity: 1,
						pricing_scheme: {
							"price": 2450,
							"scheme_type": "unit"
						}
					},
					{
						name: 'Livro Yellowbook',
						description: 'Livro Yellowbook',
						quantity: 1,
						cycles: 1,
						pricing_scheme: {
							"price": 13990,
							"scheme_type": "unit"
						}
					}
				]
			}
		});

		planosCriados.lista.push({
			id: dataPlanoMensalComBook.id,
			name: dataPlanoMensalComBook.name
		});

		console.log( ` --- Plano cadastrado ${dataPlanoMensalComBook.name} ${dataPlanoMensalComBook.id}` );

		res.status( 201 ).json( planosCriados );
	}
	catch ( e ) {
		console.log( ' !!! Erro cadastrando planos', e.message );
		console.log( ' !!! Erro cadastrando planos', e.config );
		console.log( ' !!! Erro cadastrando planos', e.response.data );
		res.status( 500 ).json({ message: 'Erro no cadastrando de planos.' });
	}
};

exports.listar = async ( req, res ) => {
	try {
		const { data } = await http({
			method: 'GET',
			path: `/plans?page=${req.params.pagina || 1}&size=20`
		});

		res.status( 200 ).json( data );
	}
	catch ( e ) {
		console.log( ' !!! Erro listando planos', e );
		res.status( 500 ).json({ message: 'Erro na lista de planos.' });
	}
};
