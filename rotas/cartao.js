const { http } = require( '../lib/mundipagg.js' );

exports.listar = async ( req, res ) => {
	try {
		const { data } = await http({
			method: 'GET',
			path: `/customers/${req.params.usuarioId}/cards`
		});

		res.status( 200 ).json( data );
	}
	catch ( e ) {
		console.log( ' !!! Erro listando cartões', e );
		res.status( 500 ).json({ message: 'Erro na lista de cartões.' });
	}
};
