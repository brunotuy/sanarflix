const { http } = require( '../lib/mundipagg.js' );

exports.cadastro = async ( req, res ) => {
	const { cliente, cartao, produtos } = req.body || {};
	const { nome, email } = cliente || {};
	const { numero, expiracao_mes, expiracao_ano, cvv } = cartao || {};

	if ( !nome || !email || !numero || !expiracao_mes || !expiracao_ano || !cvv ) {
		res.status( 500 ).json({ message: 'Entrada incompleta.' });
	}

	try {
		const { data } = await http({
			method: 'POST',
			path: '/customers',
			data: {
				type: 'individual',
				name: nome,
				email: email
			}
		});

		const { plano_id } = produtos.find( produto => produto.tipo === "plano" ) || {};

		if ( plano_id ) {
			const { data: assinatura } = await http({
				method: 'POST',
				path: '/subscriptions',
				data: {
					plan_id: plano_id,
					customer_id: data.id,
					payment_method: 'credit_card',
					card: {
						cvv: cvv,
						number: numero,
						exp_year: expiracao_ano,
						exp_month: expiracao_mes,
						holder_name: nome.toUpperCase()
					}
				}
			});

			db.assinaturas.save( assinatura );
		}

		res.sendStatus( 202 );
	}
	catch ( e ) {
		console.log( ' !!! Erro criando o usuário', e );
		res.status( 500 ).json({ message: 'Erro na criação do usuário.' });
	}
};

exports.listar = async ( req, res ) => {
	try {
		const { data } = await http({
			method: 'GET',
			path: '/customers'
		});

		res.status( 200 ).json( data );
	}
	catch ( e ) {
		console.log( ' !!! Erro listando usuários', e );
		res.status( 500 ).json({ message: 'Erro na lista de usuários.' });
	}
};
