const { http } = require( '../lib/mundipagg.js' );

exports.cancelar = ( req, res ) => {
	db.assinaturas.findOne(
		{"customer.id": req.params.usuarioId},
		{id: 1},
		( erro, resultado ) => {
			if ( erro ) {
				res.status( 500 ).json({ message: 'Erro buscando assinatura.' });
			}

			else if ( !resultado ) {
				res.status( 500 ).json({ message: 'Não foi encontrada assinatura desse cliente.' });
			}

			else {
				http({
					method: 'DELETE',
					path: `/subscriptions/${resultado.id}`
				})
					.then( ( a ) => { console.log( a ); res.sendStatus( 200 ); })
					.catch( ( e ) => {
						console.log( ' !!! Erro cancelando assinatura', e );
						res.status( 500 ).json({ message: 'Erro cancelando assinatura.' });
					});
			}
		}
	);
};

exports.editarCartao = ( req, res ) => {
	const { cliente_id, cartao } = req.body || {};
	const { numero, expiracao_mes, expiracao_ano, cvv } = cartao || {};

	if ( !cliente_id || !numero || !expiracao_mes || !expiracao_ano || !cvv ) {
		res.status( 500 ).json({ message: 'Entrada incompleta.' });
	}

	db.assinaturas.findOne(
		{"customer.id": cliente_id},
		{
			id: 1,
			"card.id": 1,
			"card.holder_name": 1
		},
		( erro, resultado ) => {
			if ( erro ) {
				res.status( 500 ).json({ message: 'Erro buscando assinatura.' });
			}

			else if ( !resultado ) {
				res.status( 500 ).json({ message: 'Não foi encontrada assinatura desse cliente.' });
			}

			else {
				console.log( resultado );
				http({
					method: 'PATCH',
					path: `/subscriptions/${resultado.id}/card`,
					data: {
						card_id: resultado.card.id,
						card: {
							cvv,
							number: numero,
							exp_year: expiracao_ano,
							exp_month: expiracao_mes,
							holder_name: resultado.card.holder_name
						}
					}
				})
					.then( ( a ) => { console.log( a ); res.sendStatus( 200 ); })
					.catch( ( e ) => {
						console.log( ' !!! Erro atualizando cartão da assinatura', e );
						res.status( 500 ).json({ message: 'Erro na atualização do cartão da assinatura.' });
					});
			}
		}
	);
};

exports.listar = async ( req, res ) => {
	try {
		const { data } = await http({
			method: 'GET',
			path: '/subscriptions'
		});

		res.status( 200 ).json( data );
	}
	catch ( e ) {
		console.log( ' !!! Erro listando assinaturas', e );
		res.status( 500 ).json({ message: 'Erro na lista de assinatuas.' });
	}
};
