const fs = require( 'fs' );
const objRotas = {};

fs
	.readdirSync(__dirname)
 	.filter(( file ) => (file.indexOf(".") !== 0) && (file !== "index.js") )
	.forEach(( file ) => {
		const nome = file.substring( 0, file.indexOf( '.' ) ).toUpperCase();
		const rotas = require( './'+file );

		objRotas[nome] = rotas;
	});

module.exports = objRotas;
