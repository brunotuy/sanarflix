const express = require( 'express' );
const mongojs = require( 'mongojs' );
const bodyParser = require( 'body-parser' );

global.db = mongojs( 'sanarflix', [ 'assinaturas' ]);

const rotas = require( './rotas' );
const app = express();
const porta = 3003;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post( '/Usuario', rotas.USUARIO.cadastro );
app.get( '/Usuario', rotas.USUARIO.listar );

app.get( '/Cartao/:usuarioId', rotas.CARTAO.listar );

app.post( '/Plano', rotas.PLANO.cadastro );
app.get( '/Plano', rotas.PLANO.listar );
app.get( '/Plano/:pagina', rotas.PLANO.listar );

app.get( '/Assinatura', rotas.ASSINATURA.listar );
app.patch( '/Assinatura/cartao', rotas.ASSINATURA.editarCartao );
app.delete( '/Assinatura/:usuarioId', rotas.ASSINATURA.cancelar );

app.listen( porta );
console.log( ` *** Escutando na porta ${porta}` )
